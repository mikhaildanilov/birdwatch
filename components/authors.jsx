const authors = {
  marat: {
    name: 'Марат Магомедов',
    desc: 'Член команды Horny Orni Team. Бёрдвотчер и программист.',
    photo: '/images/authors/marat.jpg'
  },
  nastya: {
    name: 'Анастасия Бесфамильная',
    desc: 'Член команды Horny Orni Team, бёрдер.',
    photo: '/images/authors/nastya.jpg'
  },
  katya: {
    name: 'Екатерина Сичинава',
    desc: 'Член команды Horny Orni Team. Студент-палеонтолог и бёрдер.',
    photo: '/images/authors/katya.jpg'
  },
  stepan: {
    name: 'Степан Сурков',
    desc: 'Редактор, бёрдер.',
    photo: '/images/authors/stepan.jpg'
  }
};

export default authors;
