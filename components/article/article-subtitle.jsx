const ArticleSubtitle = ({children}) => {
	return (
		<h3 className="title title_h3">{children}</h3>
	);
};

export default ArticleSubtitle;
