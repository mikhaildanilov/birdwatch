const ArticleOl = ({children}) => {
  return <ol className='article__ol'>{children}</ol>;
};

export default ArticleOl;
