const ArticleUl = ({children}) => {
  return <ul className='article__ul'>{children}</ul>;
};

export default ArticleUl;
