const ArticleTitleHead = ({children}) => {
  return <h1 className='title title_h1'>{children}</h1>;
};

export default ArticleTitleHead;
